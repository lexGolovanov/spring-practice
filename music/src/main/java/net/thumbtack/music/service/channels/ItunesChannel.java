package net.thumbtack.music.service.channels;

import net.thumbtack.music.model.Recording;

import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service("itunesChannel")
public class ItunesChannel implements PublishingChannels {
    @Override
    public void publish(final Recording record, final ZonedDateTime publishAvailableDate) {
        if (record.getAudioUrl() != null) {
            System.out.println(
                    String.format("Published \"%s\" by %s on iTunes.\nDate: %s",
                            record.getSongTitle(), record.getArtist(), publishAvailableDate.toString()
                    )
            );
        } else {
            System.out.println("Audio URL not found for this record. Ignored publishing on iTunes");
        }
    }
}
