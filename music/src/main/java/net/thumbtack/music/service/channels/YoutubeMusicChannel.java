package net.thumbtack.music.service.channels;

import net.thumbtack.music.model.Recording;

import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service("youtubeMusicChannel")
public class YoutubeMusicChannel implements PublishingChannels {
    @Override
    public void publish(final Recording record, final ZonedDateTime publishAvailableDate) {
        if (record.getVideoUrl() != null) {
            System.out.println(
                    String.format("Published video \"%s\" by %s on YouTubeMusic.\nDate: %s",
                            record.getSongTitle(), record.getArtist(), publishAvailableDate.toString()
                    )
            );
        } else {
            System.out.println("Video URL not found for this record. Ignored publishing video on YouTubeMusic");
        }

        if (record.getAudioUrl() != null) {
            System.out.println(
                    String.format("Published audio \"%s\" by %s on YouTubeMusic.\nDate: %s",
                            record.getSongTitle(), record.getArtist(), publishAvailableDate.toString()
                    )
            );
        } else {
            System.out.println("Audio URL not found for this record. Ignored publishing audio on YouTubeMusic");
        }
    }
}
