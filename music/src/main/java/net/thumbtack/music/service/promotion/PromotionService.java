package net.thumbtack.music.service.promotion;

import net.thumbtack.music.model.Recording;

import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service("promotionService")
public class PromotionService {
    public void createCampaign(final Recording record, final ZonedDateTime campaignCreateDate) {
        System.out.println(
                String.format("Created campaign for %s by %s. Started at %s",
                        record.getSongTitle(), record.getArtist(), campaignCreateDate.toString()
                )
        );
    }
}
