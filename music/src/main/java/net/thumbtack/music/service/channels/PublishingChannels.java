package net.thumbtack.music.service.channels;

import net.thumbtack.music.model.Recording;

import java.time.ZonedDateTime;

public interface PublishingChannels {
    void publish(Recording record, ZonedDateTime publishAvailableDate);
}
