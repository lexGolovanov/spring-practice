package net.thumbtack.music;

import net.thumbtack.music.model.Recording;
import net.thumbtack.music.service.channels.YoutubeMusicChannel;
import net.thumbtack.music.service.data.RecordingDataHub;
import net.thumbtack.music.service.channels.PublishingChannels;
import net.thumbtack.music.service.promotion.PromotionService;

import org.springframework.stereotype.Component;
import org.springframework.boot.CommandLineRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

@Component
public class CmdMusicOperations implements CommandLineRunner {
    private final RecordingDataHub dataHub;
    private final PromotionService promotionService;
    private final List<PublishingChannels> channelsList;
    private final PublishingChannels youtubeMusicChannel;

    @Autowired
    public CmdMusicOperations(
            final RecordingDataHub dataHub,
            final PromotionService promotionService,
            final List<PublishingChannels> channelsList,
            final YoutubeMusicChannel youtubeChannel) {
        this.dataHub = dataHub;
        this.promotionService = promotionService;
        this.youtubeMusicChannel = youtubeChannel;
        this.channelsList = channelsList;
    }

    private void scenario1() {
        System.out.println("\nScenario #1");
        final Recording recording = new Recording(
                "Hall & Oates",
                "Out of Touch",
                "Big Bam Boom",
                "1984",
                "/url/to/cover/bbb.png",
                "Soft-Rock",
                "4:12",
                "url/to/audio/id4815",
                "url/to/video/id162345"
        );
        final Recording savedRecording = dataHub.save(recording);
        promotionService.createCampaign(savedRecording, ZonedDateTime.now());
        channelsList.forEach(chan ->
                chan.publish(savedRecording, ZonedDateTime.now().plus(1, ChronoUnit.WEEKS))
        );
    }

    private void scenario2() {
        System.out.println("\nScenario #2");
        final Recording recording = new Recording(
                "Phil Collins",
                "In the Air Tonight",
                "Face Value",
                "1981",
                "/url/to/cover/face_value.jpg",
                "Soft-Rock",
                "5:29",
                null,
                "url/to/video/id108"
        );
        final Recording savedRecording = dataHub.save(recording);
        promotionService.createCampaign(savedRecording, ZonedDateTime.now());
        youtubeMusicChannel.publish(savedRecording, ZonedDateTime.now().plus(2, ChronoUnit.WEEKS));
    }

    @Override
    public void run(String... args) {
        scenario1();
        scenario2();
    }
}
